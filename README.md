**Academy Theme for Ghost CMS**

The Academy theme is a meticulously designed theme for Ghost CMS, tailored specifically for academic, educational, and personal development blogs. Emphasizing clarity, ease of navigation, and a professional look, it's perfect for institutions, educators, and anyone looking to share knowledge or educational content.

**Features**

- **Responsive Design:** Ensures your site looks great on all devices.
- **Custom Pages:** Includes templates for About, Contact, and a custom Home page.
- **SEO Friendly:** Optimized for search engines to help your content reach a wider audience.
- **Easy to Customize:** With well-organized assets and partials, personalizing your theme is a breeze.


**Installation**

**1. Download the Theme:** Start by downloading the academy.zip file from the theme repository or marketplace.

**2. Upload to Ghost**: Navigate to your Ghost CMS's Design settings, and under Themes, click on Upload a Theme. Choose the academy.zip file.

**3. Activate the Theme:** Once uploaded, you'll see the Academy theme listed among your themes. Click Activate to set it as your current theme.


**Configuration**

 **Setting Up Pages**
 After installing the Academy theme you'll want to set up your custom pages:

- **About Page:** Create a new page in Ghost admin titled About and use the page-about.hbs template from the theme.
- **Contact Page**: Similarly, create a Contact page and assign the page-contact.hbs template.
- **Custom Home Page:** To utilize the custom Home page layout, create a page titled Home and assign the page-home.hbs template.


**Customizing the Theme**

You can customize the theme's look and feel by editing the CSS within the **assets** folder. For more detailed customizations, including layout changes, modify the Handlebars template files (***.hbs**) directly.

**Theme Structure**

- **assets/:** Contains all static resources like CSS, JS, and images.
- **default.hbs:** The base template from which all other templates inherit.
- **index.hbs:** Template for the home page showing a list of posts.
- **page-*.hbs:** Specific templates for custom pages (About, Contact, Home).
- **partials/:** Reusable pieces of code that can be included in other Handlebars templates.
- **post.hbs:** Template for individual blog posts.

**Support**
For support, questions, or to report issues, please visit the theme repository issue tracker.
